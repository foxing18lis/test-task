The app contains a database for the insurance company.

To run the application you need:
1. in the console in the root folder of the project, enter the command: `docker-compose up`  ;
2. to stop the application, enter this command in the console: `docker-compose down` .

To test the application use Postman.
There are 6 different entities in the application, for each of them, the ability to add a new record and get all records is implemented.

| entity | url | method | request | response |
|--------|-----|--------|---------|----------|
|Client|http://localhost:8080/client|Post| { "name":"name",  "surname":"surname",  "passportId":"7593bjf00"}|{ "id":1,   "name":"name",  "surname":"surmane",  "passportId":"7593bjf00"}|
|Client|http://localhost:8080/client|Get| |[{"id":1,   "name":"name",  "surname":"surname",  "passportId":"7593bjf00"}]|
|ObjectType (e.g.house, car)|http://localhost:8080/objectType|Post|{ "title":"house"}|{ "id":2,"title":"house"}|
|ObjectType|http://localhost:8080/objectType|Get| |[{ "id":2,"title":"house"}]|
|InsuranceObject(insurance object with a unique identifier, for example, this is a number for a car, an address for a house)|http://localhost:8080/insuranceObject|Post|{"objectTypeId":3,  "identification":"433533"}| { "id": 4, "identification": "i440204--234","objectType": {"id": 3, "title": "title"}}|
|InsuranceObject|http://localhost:8080/insuranceObject|Get||[ { "id": 4, "identification": "i440204--234","objectType": {"id": 3, "title": "title"}}]|
|Coverage|http://localhost:8080/coverage|Post|{"title": "title"}|{"id":5,"title": "title"}|
|Coverage|http://localhost:8080/coverage|Get| |[{"id":5,"title": "title"}]|
|Contract(contains the content of the contract for Coverage)|http://localhost:8080/contract|Post|{"coverageId":1,"content":"content"}|{"id":8,"coverage": {"id": 1,"title": "title"},"content":"content"}|
|Contract|http://localhost:8080/contract|Get| |[{"id":8,"coverage": {"id": 1,"title": "title"},"content":"content"}]|
|PersonalPolicy (same as tariff)|http://localhost:8080/policy|Post|{"clientId":2,"insuranceObjectId":4,"coverageId":1}|{"id":10,"clientId":2,"insuranceObjectId":4,"coverageId":1}|
|PersonalPolicy|http://localhost:8080/policy|Get| |[{"client": "id":1,   "name":"name",  "surname":"surname",  "passportId":"7593bjf00"}, "insuranceObject": { "id": 4, "identification": "i440204--234","objectType": {"id": 3, "title": "title"}},"coverage": {"id": 1,"title": "title"}}]|



