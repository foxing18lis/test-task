package com.test.task.repositories;

import com.test.task.models.InsuranceObject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface InsuranceObjectRepository extends JpaRepository<InsuranceObject, Long> {

    Optional<InsuranceObject> findById(Long id);

    List<InsuranceObject> findAll();

    InsuranceObject save(InsuranceObject policy);
}
