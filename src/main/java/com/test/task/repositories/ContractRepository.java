package com.test.task.repositories;

import com.test.task.models.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ContractRepository extends JpaRepository<Contract, Long> {

    @Query("select c from Contract c where c.notValidSince is null and c.id = :id")
    Optional<Contract> findById(@Param("id") Long id);

    @Query("select c from Contract c where c.notValidSince is null")
    List<Contract> findAll();

    Contract save(Contract policy);
}
