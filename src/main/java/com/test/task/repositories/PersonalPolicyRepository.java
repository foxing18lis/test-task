package com.test.task.repositories;

import com.test.task.models.PersonalPolicy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonalPolicyRepository extends JpaRepository<PersonalPolicy, Long> {

    List<PersonalPolicy> findAll();

    PersonalPolicy save(PersonalPolicy policy);
}
