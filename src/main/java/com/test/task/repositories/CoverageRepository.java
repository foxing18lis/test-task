package com.test.task.repositories;

import com.test.task.models.Coverage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CoverageRepository extends JpaRepository<Coverage, Long> {

    Optional<Coverage> findById(Long id);

    List<Coverage> findAll();

    Coverage save(Coverage policy);
}
