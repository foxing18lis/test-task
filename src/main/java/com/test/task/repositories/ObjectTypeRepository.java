package com.test.task.repositories;

import com.test.task.models.ObjectType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ObjectTypeRepository extends JpaRepository<ObjectType, Long> {

    Optional<ObjectType> findById(Long id);

    List<ObjectType> findAll();

    ObjectType save(ObjectType policy);
}
