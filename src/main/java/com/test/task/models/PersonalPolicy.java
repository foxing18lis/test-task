package com.test.task.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class PersonalPolicy {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @ManyToOne
    @Getter
    @Setter
    private Client client;

    @ManyToOne
    @Getter
    @Setter
    private InsuranceObject insuranceObject;

    @ManyToOne
    @Getter
    @Setter
    private Coverage coverage;

    protected PersonalPolicy() {}

    public PersonalPolicy(Client client, InsuranceObject insuranceObject, Coverage coverage) {
        this.client = client;
        this.coverage = coverage;
        this.insuranceObject = insuranceObject;
    }
}
