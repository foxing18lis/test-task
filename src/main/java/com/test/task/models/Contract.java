package com.test.task.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String content;

    @ManyToOne
    @Getter
    @Setter
    private Coverage coverage;

    @Getter
    @Setter
    private Date notValidSince = null;

    protected Contract() {}

    public Contract(Coverage coverage, String content) {
        this.content = content;
        this.coverage = coverage;
    }
}
