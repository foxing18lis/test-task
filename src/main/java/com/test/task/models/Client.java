package com.test.task.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

@Entity
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @NotEmpty(message = "First name is required")
    private String name;

    @Getter
    @Setter
    @NotEmpty(message = "Surname is required")
    private String surname;

    @Getter
    @Setter
    @NotEmpty(message = "PassportId is required")
    private String passportId;

    protected Client() {}

    public Client(String name, String surname, String passportId) {
        this.name = name;
        this.surname = surname;
        this.passportId = passportId;
    }
}
