package com.test.task.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class Coverage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String title;

    protected Coverage() {}

    public Coverage(String title) {
        this.title = title;
    }
}
