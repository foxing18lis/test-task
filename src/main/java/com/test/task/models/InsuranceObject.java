package com.test.task.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class InsuranceObject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String identification;

    @Getter
    @Setter
    @ManyToOne
    private ObjectType objectType;

    protected InsuranceObject() {}

    public InsuranceObject(String identification, ObjectType objectType) {
        this.objectType = objectType;
        this.identification = identification;
    }
}
