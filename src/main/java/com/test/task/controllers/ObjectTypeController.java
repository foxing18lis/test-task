package com.test.task.controllers;

import com.test.task.dtos.CoverageAddingDto;
import com.test.task.dtos.CoverageDto;
import com.test.task.dtos.ObjectTypeAddingDto;
import com.test.task.dtos.ObjectTypeDto;
import com.test.task.services.CoverageService;
import com.test.task.services.ObjectTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/objectType")
public class ObjectTypeController {

    private ObjectTypeService service;

    @Autowired
    public ObjectTypeController(ObjectTypeService service) {
        this.service = service;
    }

    @GetMapping
    public List<ObjectTypeDto> findAll() {
        return service.findAllAndTransform();
    }

    @PostMapping
    public ResponseEntity<?> addObjectType(@RequestBody ObjectTypeAddingDto addingDto) {
        try{
            ObjectTypeDto dto = service.addObjectType(addingDto);

            return new ResponseEntity<>(dto, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
