package com.test.task.controllers;

import com.test.task.dtos.InsuranceObjectAddingDto;
import com.test.task.dtos.InsuranceObjectDto;
import com.test.task.services.InsuranceObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/insuranceObject")
public class InsuranceObjectController {

    private InsuranceObjectService service;

    @Autowired
    public InsuranceObjectController(InsuranceObjectService service) {
        this.service = service;
    }

    @GetMapping
    public List<InsuranceObjectDto> findAll() {
        return service.findAllAndTransform();
    }

    @PostMapping
    public ResponseEntity<?> addInsuranceObject(@RequestBody InsuranceObjectAddingDto addingDto) {
        try{
            InsuranceObjectDto dto = service.addInsuranceObject(addingDto);

            return new ResponseEntity<>(dto, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
