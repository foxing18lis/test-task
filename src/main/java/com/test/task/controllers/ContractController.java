package com.test.task.controllers;

import com.test.task.dtos.ContractAddingDto;
import com.test.task.dtos.ContractDto;
import com.test.task.services.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/contract")
public class ContractController {

    private ContractService service;

    @Autowired
    public ContractController(ContractService service) {
        this.service = service;
    }

    @GetMapping
    public List<ContractDto> findAll() {
        return service.findAllAndTransform();
    }

    @PostMapping
    public ResponseEntity<?> addContract(@RequestBody ContractAddingDto addingDto) {
        try{
            ContractDto dto = service.addContract(addingDto);

            return new ResponseEntity<>(dto, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
