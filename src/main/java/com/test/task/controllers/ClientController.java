package com.test.task.controllers;

import com.test.task.dtos.ClientAddingDto;
import com.test.task.dtos.ClientDto;
import com.test.task.dtos.PersonalPolicyAddingDto;
import com.test.task.dtos.PersonalPolicyDto;
import com.test.task.models.Client;
import com.test.task.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/client")
public class ClientController {

    private ClientService service;

    @Autowired
    public ClientController(ClientService service) {
        this.service = service;
    }

    @GetMapping
    public List<ClientDto> findAll() {
        return service.findAllAndTransform();
    }

    @PostMapping
    public ResponseEntity<?> addClient(@RequestBody ClientAddingDto addingDto) {
        try{
            ClientDto dto = service.addClient(addingDto);

            return new ResponseEntity<>(dto, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
