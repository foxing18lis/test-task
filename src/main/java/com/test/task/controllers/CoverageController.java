package com.test.task.controllers;

import com.test.task.dtos.CoverageAddingDto;
import com.test.task.dtos.CoverageDto;
import com.test.task.services.CoverageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/coverage")
public class CoverageController {

    private CoverageService service;

    @Autowired
    public CoverageController(CoverageService service) {
        this.service = service;
    }

    @GetMapping
    public List<CoverageDto> findAll() {
        return service.findAllAndTransform();
    }

    @PostMapping
    public ResponseEntity<?> addCoverage(@RequestBody CoverageAddingDto addingDto) {
        try{
            CoverageDto dto = service.addCoverage(addingDto);

            return new ResponseEntity<>(dto, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
