package com.test.task.controllers;

import com.test.task.dtos.PersonalPolicyAddingDto;
import com.test.task.dtos.PersonalPolicyDto;
import com.test.task.services.PersonalPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/policy")
public class PersonalPolicyController {

    private PersonalPolicyService service;

    @Autowired
    public PersonalPolicyController(PersonalPolicyService service) {
        this.service = service;
    }

    @GetMapping
    public List<PersonalPolicyDto> findAll() {
        return service.findAllAndTransform();
    }

    @PostMapping
    public ResponseEntity<?> addPolicy(@RequestBody PersonalPolicyAddingDto addingDto) {
        try{
            PersonalPolicyDto dto = service.addPolicy(addingDto);

            return new ResponseEntity<>(dto, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
