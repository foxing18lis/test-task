package com.test.task.services;

import com.test.task.dtos.PersonalPolicyAddingDto;
import com.test.task.dtos.PersonalPolicyDto;
import com.test.task.models.Client;
import com.test.task.models.Coverage;
import com.test.task.models.InsuranceObject;
import com.test.task.models.PersonalPolicy;
import com.test.task.repositories.PersonalPolicyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonalPolicyService {

    private final PersonalPolicyRepository personalPolicyRepository;
    private ClientService clientService;
    private InsuranceObjectService insuranceObjectService;
    private CoverageService coverageService;

    @Autowired
    public PersonalPolicyService(final PersonalPolicyRepository repository,
                                 ClientService clientService,
                                 InsuranceObjectService insuranceObjectService,
                                 CoverageService coverageService) {
        this.personalPolicyRepository = repository;
        this.clientService = clientService;
        this.insuranceObjectService = insuranceObjectService;
        this.coverageService = coverageService;
    }

    public List<PersonalPolicy> findAll() {
        return personalPolicyRepository.findAll();
    }

    public List<PersonalPolicyDto> findAllAndTransform() {
        List<PersonalPolicy> policyList = this.findAll();
        List<PersonalPolicyDto> dto = new ArrayList<>();

        for (PersonalPolicy policy : policyList) {
            dto.add(this.transform(policy));
        }

        return dto;
    }

    public PersonalPolicyDto transform(PersonalPolicy policy) {
        return new PersonalPolicyDto(policy.getClient(), policy.getInsuranceObject(), policy.getCoverage());
    }

    public PersonalPolicyDto addPolicy(PersonalPolicyAddingDto dto) throws Exception {
        Client client = clientService.findById(dto.getClientId());
        InsuranceObject insuranceObject = insuranceObjectService.findById(dto.getInsuranceObjectId());
        Coverage coverage = coverageService.findById(dto.getCoverageId());

        if (client != null && insuranceObject != null && coverage != null) {
            PersonalPolicy policy = new PersonalPolicy(client, insuranceObject, coverage);
            personalPolicyRepository.save(policy);

            return this.transform(policy);
        } else {
            throw new Exception("Something wrong...");
        }
    }
}
