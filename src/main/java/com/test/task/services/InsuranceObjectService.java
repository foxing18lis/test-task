package com.test.task.services;

import com.test.task.dtos.InsuranceObjectAddingDto;
import com.test.task.dtos.InsuranceObjectDto;
import com.test.task.models.*;
import com.test.task.repositories.InsuranceObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InsuranceObjectService {

    private final InsuranceObjectRepository insuranceObjectRepository;
    private ObjectTypeService objectTypeService;

    @Autowired
    public InsuranceObjectService(final InsuranceObjectRepository repository,
                                  ObjectTypeService service) {
        this.insuranceObjectRepository = repository;
        this.objectTypeService = service;
    }

    public InsuranceObject findById(Long id) {
        Optional<InsuranceObject> insuranceObject = insuranceObjectRepository.findById(id);

        return insuranceObject.orElse(null);
    }

    public List<InsuranceObject> findAll() {
        return insuranceObjectRepository.findAll();
    }

    public List<InsuranceObjectDto> findAllAndTransform() {
        List<InsuranceObject> insuranceObjects = this.findAll();
        List<InsuranceObjectDto> dto = new ArrayList<>();

        for (InsuranceObject insuranceObject : insuranceObjects) {
            dto.add(this.transform(insuranceObject));
        }

        return dto;
    }

    public InsuranceObjectDto transform(InsuranceObject insuranceObject) {
        return new InsuranceObjectDto(insuranceObject.getId(),
                insuranceObject.getObjectType(), insuranceObject.getIdentification());
    }

    public InsuranceObjectDto addInsuranceObject(InsuranceObjectAddingDto dto) throws Exception {
        ObjectType objectType = objectTypeService.findById(dto.getObjectTypeId());

        if (objectType != null) {
            InsuranceObject insuranceObject = new InsuranceObject(dto.getIdentification(), objectType);
            insuranceObjectRepository.save(insuranceObject);

            return this.transform(insuranceObject);
        } else {
            throw new Exception("Something wrong...");
        }
    }
}
