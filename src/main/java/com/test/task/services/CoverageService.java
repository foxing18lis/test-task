package com.test.task.services;

import com.test.task.dtos.CoverageAddingDto;
import com.test.task.dtos.CoverageDto;
import com.test.task.models.Coverage;
import com.test.task.repositories.CoverageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CoverageService {

    private final CoverageRepository coverageRepository;

    @Autowired
    public CoverageService(final CoverageRepository repository) {
        this.coverageRepository = repository;
    }

    public Coverage findById(Long id) {
        Optional<Coverage> coverage = coverageRepository.findById(id);

        return coverage.orElse(null);
    }

    public List<Coverage> findAll() {
        return coverageRepository.findAll();
    }

    public List<CoverageDto> findAllAndTransform() {
        List<Coverage> coverages = this.findAll();
        List<CoverageDto> dto = new ArrayList<>();

        for (Coverage coverage : coverages) {
            dto.add(this.transform(coverage));
        }

        return dto;
    }

    public CoverageDto transform(Coverage coverage) {
        return new CoverageDto(coverage.getId(), coverage.getTitle());
    }

    public CoverageDto addCoverage(CoverageAddingDto dto) {
        Coverage coverage = new Coverage(dto.getTitle());
        coverageRepository.save(coverage);

        return this.transform(coverage);
    }
}
