package com.test.task.services;

import com.test.task.dtos.ObjectTypeAddingDto;
import com.test.task.dtos.ObjectTypeDto;
import com.test.task.models.ObjectType;
import com.test.task.repositories.ObjectTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ObjectTypeService {

    private final ObjectTypeRepository objectTypeRepository;

    @Autowired
    public ObjectTypeService(final ObjectTypeRepository repository) {
        this.objectTypeRepository = repository;
    }

    public ObjectType findById(Long id) {
        Optional<ObjectType> objectType = objectTypeRepository.findById(id);

        return objectType.orElse(null);
    }

    public List<ObjectType> findAll() {
        return objectTypeRepository.findAll();
    }

    public List<ObjectTypeDto> findAllAndTransform() {
        List<ObjectType> objectTypes = this.findAll();
        List<ObjectTypeDto> dto = new ArrayList<>();

        for (ObjectType objectType : objectTypes) {
            dto.add(this.transform(objectType));
        }

        return dto;
    }

    public ObjectTypeDto transform(ObjectType objectType) {
        return new ObjectTypeDto(objectType.getId(), objectType.getTitle());
    }

    public ObjectTypeDto addObjectType(ObjectTypeAddingDto dto) {
        ObjectType objectType = new ObjectType(dto.getTitle());
        objectTypeRepository.save(objectType);

        return this.transform(objectType);
    }
}
