package com.test.task.services;

import com.test.task.dtos.ContractAddingDto;
import com.test.task.dtos.ContractDto;
import com.test.task.models.Contract;
import com.test.task.models.Coverage;
import com.test.task.repositories.ContractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContractService {

    private final ContractRepository contractRepository;
    private CoverageService coverageService;

    @Autowired
    public ContractService(final ContractRepository repository,
                           CoverageService coverageService) {
        this.contractRepository = repository;
        this.coverageService = coverageService;
    }

    public List<Contract> findAll() {
        return contractRepository.findAll();
    }

    public List<ContractDto> findAllAndTransform() {
        List<Contract> contracts = this.findAll();
        List<ContractDto> dto = new ArrayList<>();

        for (Contract contract : contracts) {
            dto.add(this.transform(contract));
        }

        return dto;
    }

    public ContractDto transform(Contract contract) {
        return new ContractDto(contract.getId(), contract.getCoverage(), contract.getContent());
    }

    public ContractDto addContract(ContractAddingDto dto) throws Exception {
        Coverage coverage = coverageService.findById(dto.getCoverageId());

        if (coverage != null) {
            Contract contract = new Contract(coverage, dto.getContent());
            contractRepository.save(contract);

            return this.transform(contract);
        } else {
            throw new Exception("Something wrong...");
        }
    }
}
