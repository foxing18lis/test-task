package com.test.task.services;

import com.test.task.dtos.ClientAddingDto;
import com.test.task.dtos.ClientDto;
import com.test.task.models.Client;
import com.test.task.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClientService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(final ClientRepository repository) {
        this.clientRepository = repository;
    }

    public Client findById(Long id) {
        Optional<Client> client = clientRepository.findById(id);

        return client.orElse(null);
    }

    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    public List<ClientDto> findAllAndTransform() {
        List<Client> clients = this.findAll();
        List<ClientDto> dto = new ArrayList<>();

        for (Client client : clients) {
            dto.add(this.transform(client));
        }

        return dto;
    }

    public ClientDto transform(Client client) {
        return new ClientDto(client.getId(), client.getName(), client.getSurname(), client.getPassportId());
    }

    public ClientDto addClient(ClientAddingDto dto) {
        Client client = new Client(dto.getName(), dto.getSurname(), dto.getPassportId());
        clientRepository.save(client);

        return this.transform(client);
    }
}
