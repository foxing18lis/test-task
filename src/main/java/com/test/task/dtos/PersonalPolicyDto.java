package com.test.task.dtos;

import com.test.task.models.Client;
import com.test.task.models.Coverage;
import com.test.task.models.InsuranceObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PersonalPolicyDto {

    private Client client;
    private InsuranceObject insuranceObject;
    private Coverage coverage;
}
