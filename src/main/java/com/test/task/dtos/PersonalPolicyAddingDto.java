package com.test.task.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PersonalPolicyAddingDto {

    private Long clientId;
    private Long insuranceObjectId;
    private Long coverageId;
}
