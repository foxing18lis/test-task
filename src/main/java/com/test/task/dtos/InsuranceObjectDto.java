package com.test.task.dtos;

import com.test.task.models.ObjectType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class InsuranceObjectDto {

    private Long id;
    private ObjectType objectType;
    private String identification;
}
