package com.test.task.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClientAddingDto {

    private String name;
    private String surname;
    private String passportId;
}
