package com.test.task.dtos;

import com.test.task.models.Coverage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ContractDto {

    private Long id;
    private Coverage coverage;
    private String content;
}
