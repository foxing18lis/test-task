package com.test.task.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class InsuranceObjectAddingDto {

    private Long objectTypeId;
    private String identification;
}
